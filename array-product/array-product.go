package main

import (
	"fmt"
)

func main() {
	a := []int{1, 2, 3, 4, 5}

	if len(a) == 0 {
		return
	}

	lp := make([]int, len(a))
	rp := make([]int, len(a))
	p := make([]int, len(a))

	prod := a[0]
	lp[0] = a[0]
	for i := 1; i < len(a); i++ {
		prod *= a[i]
		lp[i] = prod
	}

	prod = a[len(a)-1]
	rp[len(a)-1] = a[len(a)-1]
	prod = a[len(a)-1]
	for i := len(a) - 2; i >= 0; i-- {
		prod *= a[i]
		rp[i] = prod
	}

	fmt.Println(a)
	fmt.Println(lp)
	fmt.Println(rp)

	p[0] = rp[1]
	p[len(a)-1] = lp[len(a)-2]

	for i := 0; i < len(a)-1; i++ {
		if i == 0 {
			p[0] = rp[0]
			continue
		}
		p[i] = lp[i-1] * rp[i+1]
	}

	fmt.Println(p)
}
