package main

import (
	"fmt"
	"math"
)

type level struct {
	height int
	values []int
}

type binaryTree struct {
	arr []*level
}

func (b *binaryTree) appendValue(val int) {
	numLevels := len(b.arr)
	if numLevels == 0 {
		rootLevel := make([]int, 0, 1)
		rootLevel = append(rootLevel, val)
		l := &level{height: 0, values: rootLevel}
		b.arr = append(b.arr, l)
		return
	}

	if b.arr[numLevels-1] == nil {
		newArray := []int{val}
		b.arr = append(b.arr, &level{height: numLevels, values: newArray})
		return
	}

	if len(b.arr[numLevels-1].values) < int(math.Pow(2, float64(numLevels-1))) {
		b.arr[numLevels-1].values = append(b.arr[numLevels-1].values, val)
	} else {
		newArray := []int{val}
		b.arr = append(b.arr, &level{height: numLevels + 1, values: newArray})
		return
	}
}

func (b *binaryTree) String() string {
	out := "{\n"

	for _, i := range b.arr {
		out += fmt.Sprintf("\t%v,\n", i.values)
	}
	out += "}"

	return out
}

func main() {

	b := &binaryTree{}

	for i := 0; i < 10; i++ {
		b.appendValue(i)
	}
	fmt.Println(b)
}
