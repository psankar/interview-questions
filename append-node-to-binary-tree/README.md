Add an element to a binary tree, such that the newly added element is the last element in the last level. Create a new level if all the nodes in the current level are full.

IOW, build a left-complete-binary-tree.

TAGS: BINARY_TREE, ARRAYS
