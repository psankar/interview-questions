package main

import "log"

func main() {

	log.SetFlags(log.Lshortfile)

	a := []int{1, 5, 3, 3, 4, 2, 1}
	i := 4 // 5-1, say 5 is the person index
	t := 0

	if a[i] == 0 {
		log.Println(0)
		return
	}

	for {
		// log.Println(a, t)
		for j := 0; j < len(a); j++ {
			if j == i && a[i] == 1 {
				t++
				log.Println(t)
				return
			}

			if a[j] != 0 {
				a[j]--
				t++
			}
		}
	}
}
