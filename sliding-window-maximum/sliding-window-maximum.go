package main

import "fmt"

func main() {
	num := []int{1, 3, -1, -3, 5, 3, 6, 7}
	k := 3

	for i := 0; (i + k) <= len(num); i++ {
		fmt.Print(num[i], "\t")
		max := num[i]
		for j := i + 1; j < i+k; j++ {
			fmt.Print(num[j], "\t")
			if num[j] > max {
				max = num[j]
			}
		}
		fmt.Println("\t\t\t", max)
	}
}
