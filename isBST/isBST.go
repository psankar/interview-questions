package main

import (
	"log"
	"math"
)

type node struct {
	Value int
	Left  *node
	Right *node
}

func checkBST(n *node, min, max int) bool {

	if n == nil {
		return true
	}

	log.Println(n.Value, min, max)
	if n.Value < min || n.Value > max {
		return false
	}

	if n.Left == nil && n.Right == nil {
		return true
	}

	if n.Left != nil {
		if !checkBST(n.Left, min, n.Value) {
			return false
		}
	}

	if n.Right != nil {
		return checkBST(n.Right, n.Value, max)
	}

	return true
}

func isBST(root *node) bool {
	return checkBST(root.Left, math.MinInt64, root.Value) &&
		checkBST(root.Right, root.Value, math.MaxInt64)
}

func main() {
	root := &node{
		Value: 3,
		Left: &node{
			Value: 2,
			Left: &node{
				Value: 1,
				Left:  nil,
				Right: nil,
			},
			Right: &node{
				Value: 4,
				Left:  nil,
				Right: nil,
			},
		},
		Right: &node{
			Value: 5,
			Left:  nil,
			Right: nil,
		},
	}

	log.Println(isBST(root))
}
