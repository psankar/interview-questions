package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
)

/*

123*12

123
123
123
123
123
123
123


*/

func add(num1 string, num2 string) string {

	i, j := len(num1), len(num2)
	out := ""

	carry := 0
	for i > 0 && j > 0 {

		u, uerr := strconv.Atoi(num1[i-1 : i])
		if uerr != nil {
			log.Println("ERRRRRRROR:", uerr)
			return ""
		}

		l, lerr := strconv.Atoi(num2[j-1 : j])
		if lerr != nil {
			log.Println("EREREREREREROR", lerr)
			return ""
		}

		result := u + l + carry
		if result >= 10 {
			result = result % 10
			carry = 1
		} else {
			carry = 0
		}

		out = fmt.Sprintf("%d%s", result, out)
		// log.Println("\t\t", u, l, carry, result, out)

		i--
		j--
	}

	for i > 0 && carry != 0 {

		u, uerr := strconv.Atoi(num1[i-1 : i])
		if uerr != nil {
			log.Println("ERRRRRRROR:", uerr)
			return ""
		}

		result := u + carry
		if result >= 10 {
			result = result % 10
			carry = 1
		} else {
			carry = 0
		}

		out = fmt.Sprintf("%d%s", result, out)
		i--
	}

	if i > 0 {
		out = num1[0:i] + out
	}

	if carry == 1 {
		out = "1" + out
	}

	return out
}

func multiply(num1 string, num2 string) string {

	if num1 == "0" || num2 == "0" {
		return "0"
	}

	prevTotal := ""

	for l := 0; l < len(num1); l++ {
		prevTotal += "0"
	}

	power := 0

	for i := len(num2); i > 0; i-- {
		n, nerr := strconv.Atoi(num2[i-1 : i])
		if nerr != nil {
			log.Println(nerr)
			return ""
		}
		times := int(math.Pow(10, float64(power))) * n

		power++
		// log.Println("Adding ", times, " times")
		for k := 1; k <= times; k++ {
			prevTotal = add(prevTotal, num1)
			// log.Println(prevTotal)
		}

		// log.Println("-----")

	}

	return prevTotal
}

func main() {
	fmt.Println(multiply("123456789", "987654321"))
}
