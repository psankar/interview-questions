Display a binary tree from the left most node to the right most node. For example, if the left-most node, say N1, starts at x co-ordinate zero, then its parent is at x co-ordinate 1. N1's sibling is at x co-ordinate 2.

If there is more than one node at the same x-co-ordinate, sort the nodes by height. If there are multiple nodes at same height, choose randomly.

For example:

![TreeImage](tree-left-to-right/tree-left-to-right.png)

Thanks to Jesika Haria for the question.

TAGS: BINARY_TREE, RECURSION, SORTING