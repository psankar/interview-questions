package main

import (
	"fmt"
	"sort"
)

type node struct {
	Value int
	Left  *node
	Right *node
}

var m map[int][]int

func calculate(i *node, xpos int) {

	if m[xpos] == nil {
		m[xpos] = []int{}
	}

	m[xpos] = append(m[xpos], i.Value)

	if i.Left != nil {
		calculate(i.Left, xpos-1)
	}
	if i.Right != nil {
		calculate(i.Right, xpos+1)
	}
}

func main() {

	root := &node{
		Value: 1,
		Left: &node{
			Value: 2,
			Left: &node{
				Value: 4,
				Left:  nil,
				Right: &node{
					Value: 9,
					Left:  nil,
					Right: nil,
				},
			},
			Right: &node{
				Value: 5,
				Left:  nil,
				Right: nil,
			},
		},
		Right: &node{
			Value: 3,
			Left:  nil,
			Right: &node{
				Value: 6,
				Left: &node{
					Value: 7,
					Left:  nil,
					Right: nil,
				},
				Right: &node{
					Value: 8,
					Left:  nil,
					Right: nil,
				},
			},
		},
	}

	m = make(map[int][]int)

	calculate(root, 0)

	// log.Println(m)

	var keys []int
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for _, k := range keys {
		for _, va := range m[k] {
			fmt.Print(va, " ")
		}
	}
	fmt.Println()
}

// Time Complexity:
// O(n) to walk all the nodes in the tree
// O(n log n) to sort the map
// So overall, the time complexity is: O(n log n)

// Additional space complexity:
// O(n) for the hash table
