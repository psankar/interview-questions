I try to capture, in this repository, some of the best programs that I have come across for asking in programming interviews. Your suggestions for questions, alternate solutions, alternate solutions in alternate languages, review comments are welcome.

All sources are licensed under the MIT License. For more details look at the LICENSE file.
