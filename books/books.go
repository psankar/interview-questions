package main

import "log"

func budgetShopping(n int32, bundleQuantities []int32,
	bundleCosts []int32) int32 {

	var totalItems int32
	var perUnitCost []float32

	for i := 0; i < len(bundleCosts); i++ {
		perUnitCost = append(perUnitCost,
			float32(bundleCosts[i])/float32(bundleQuantities[i]))
	}

	for {

		var curMinUnitCost float32
		t := -1

		for j := 0; j < len(bundleCosts); j++ {
			if n >= bundleCosts[j] {
				if t == -1 {
					curMinUnitCost = perUnitCost[j]
					t = j
				} else {
					if perUnitCost[j] < curMinUnitCost {
						curMinUnitCost = perUnitCost[j]
						t = j
					}
				}
			}
		}

		if t == -1 {
			return totalItems
		}

		totalItems += bundleQuantities[t]
		n -= bundleCosts[t]
	}
}

func main() {
	log.Println("Total number of items possible: ",
		budgetShopping(400, []int32{10, 9}, []int32{200, 1}))
}
