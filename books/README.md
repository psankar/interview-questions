Helen's school has provided her with a budget for purchasing the math notebooks her students will need. There are several stores that sell bundles of notebooks at various prices.  She can only purchase full bundles. She wants to purchase as many notebooks as she can within her budget.

 

Determine the maximum number of notebooks Helen can purchase with the amount she is given.

 

For example, Helen has n = $50 and there are m = 2 stores. The first sells bundles of 20 notebooks for cost $12 each, and the second sells bundles of only 1 notebook for $2 each. She can buy 4 bundles of 20 for $48 leaving her with $2. She can then go to the second store and purchase 1 more notebook for $2, for a total of 81 notebooks.

 

 

Function Description 

Complete the function budgetShopping in the editor below. The function must return an integer that denotes the maximum number of notebooks she can buy with n dollars.

 

budgetShopping has the following parameter(s):

    n:  integer, the number of dollars in Helen's notebook budget

    bundleQuantities:  integer array, the number of notebooks in a bundle at shop[i]

    bundleCosts:  integer array, the cost of a bundle of notebooks at shop[i]

 

Constraints

1 ≤ n ≤ 104
1 ≤ m ≤ 102
1 ≤ bundleQuantities[i] ≤ 20
1 ≤ bundleCosts[i] ≤ 200

Sample Input

50
2
20
19
2
24
20
 

Sample Output

40
 

Explanation

Helen has n = 50 dollars to purchase notebooks from the m = 2 stores described by bundleQuantities = [20, 19] and bundleCosts = [24, 20]. She makes the following purchases:

One bundle of 20 notebooks from shop 0 at a cost of 24 dollars and has n = 50 − 24 = 26 dollars left.
One bundle of 20 notebooks from shop 0 at a cost of 24 dollars and has 26 - 24 = 2 dollars left.
Helen can't afford any more notebooks, so return 20 + 20 = 40.