// 10:25 AM
package main

import "log"

func main() {
	var arr = []int{10, 5, 2, 7, 8, 7}
	k := 3

	for i := 0; (i + k) <= len(arr); i++ {
		subArr := arr[i : i+k]
		curMax := arr[i]
		for j := 1; j < len(subArr); j++ {
			if subArr[j] > curMax {
				curMax = subArr[j]
			}
		}

		log.Println(subArr, curMax)
	}
}

// 10:40 AM
