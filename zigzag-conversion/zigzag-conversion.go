package main

import (
	"fmt"
)

func convert(s string, numRows int) string {

	if s == "" {
		return s
	}

	pos := 0
	ccol := 0

	var out []([]string)
	out = make([]([]string), numRows)
	for r := range out {
		out[r] = make([]string, len(s))
	}

	for {
		row := 0
		for ; row < numRows; row++ {
			out[row][ccol] = s[pos : pos+1]
			pos++

			if pos == len(s) {
				goto end
			}
		}

		ccol++
		for k := row - 2; k > 0; k-- {
			out[k][ccol] = s[pos : pos+1]

			pos++
			if pos == len(s) {
				goto end
			}

			ccol++
		}
	}

end:
	// log.Println(ccol)
	// for row := range out {
	// log.Println(out[row])
	// }
	ret := ""
	for i := 0; i < numRows; i++ {
		for j := 0; j <= ccol; j++ {
			if t := out[i][j]; t != "" {
				ret += out[i][j]
			}
		}
	}

	return ret
}

func main() {
	fmt.Println(convert("PAYPALISHIRING", 3))
}
