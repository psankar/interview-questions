Given two strings, write a function that returns the longest common substring.

eg: substring("ABAB", "BABA") = "ABA"