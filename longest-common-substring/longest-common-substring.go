package main

import (
	"fmt"
)

func substring(s1, s2 string) string {

	prevMax := ""

	for i := 0; i < len(s1); i++ {

		matchLen := 0

		for j := 0; j < len(s2); {
			ti := i
			for {

				if j >= len(s2) || ti >= len(s1) {
					if len(s1[ti-matchLen:ti]) > len(prevMax) {
						prevMax = s1[ti-matchLen : ti]
					}
					matchLen = 0
					ti = ti - matchLen
					break
				}

				if s1[ti:ti+1] == s2[j:j+1] {
					ti++
					j++
					matchLen++
				} else {
					if matchLen > 0 {
						if len(s1[ti-matchLen:ti]) > len(prevMax) {
							prevMax = s1[ti-matchLen : ti]
						}
						matchLen = 0
						ti = ti - matchLen
					}
					j++
					break
				}
			}

		}
	}

	return prevMax
}

func main() {
	fmt.Println(substring("ABAB", "BABA"))
}
