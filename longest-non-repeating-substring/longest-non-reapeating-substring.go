package main

import "log"

func main() {
	log.Println(lengthOfLongestSubstring("peekew"))
}

func lengthOfLongestSubstring(s string) int {

	prevMaxLen := -1

	m := make(map[string]bool)
	curLen := 0

	for i := 0; i < len(s); i++ {

		c := s[i : i+1]

		log.Println(c, m[c], curLen, prevMaxLen)

		if _, found := m[c]; found {
			if curLen > prevMaxLen {
				prevMaxLen = curLen
			}

			curLen = 0
			m = make(map[string]bool)
			m[c] = true

		} else {
			curLen++
			m[c] = true
		}
	}

	if curLen > prevMaxLen {
		return curLen
	}

	return prevMaxLen
}
