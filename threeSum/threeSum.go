package main

import (
	"fmt"
	"sort"
)

func threeSum(nums []int) [][]int {

	arr := make([]([]int), 0)

	sort.Ints(nums)
	// log.Println("Checking in the array: ", nums)

	for i := 0; i <= len(nums)-3; i++ {
		if i == 0 || nums[i] > nums[i-1] {
			start := i + 1
			end := len(nums) - 1

			for {
				// log.Println("Checking: ", nums[i], nums[start], nums[end])
				sum := nums[i] + nums[start] + nums[end]

				if sum == 0 {
					// log.Println(nums[i], nums[start], nums[end])
					newArray := []int{}
					newArray = append(newArray, nums[i])
					newArray = append(newArray, nums[start])
					newArray = append(newArray, nums[end])
					arr = append(arr, newArray)
				}

				if sum > 0 {
					t := nums[end]
					for {
						end--
						if nums[end] != t || end == start {
							break
						}
					}
				} else {
					t := nums[start]
					for {
						start++
						if nums[start] != t || start == end {
							break
						}
					}
				}

				if start == end {
					break
				}
			}

		}
	}

	return arr
}

func main() {
	fmt.Println(threeSum([]int{-1, 0, 1, 2, -1, -4}))
}
