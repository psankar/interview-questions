package main

import (
	"fmt"
	"log"
)

func f(sofar []string, pending string, nest int) {
	// log.Println(sofar, pending)
	if len(pending) >= 2 {
		var x []string
		x = append(x, sofar...)
		// log.Println(nest, x)

		for i := 1; i < 3; i++ {
			var y []string
			y = append(y, x...)
			y = append(y, pending[:i])
			// log.Println("-------->", nest, y, pending[i:])
			f(y, pending[i:], nest+1)
		}
		return
	}

	if len(pending) == 1 {
		var x []string
		x = append(x, sofar...)
		x = append(x, pending)
		f(x, "", nest+1)
		return
	}

	for _, i := range sofar {
		fmt.Print(m[i], " ")
	}

	fmt.Println()
}

var m map[string]string

func main() {

	m = make(map[string]string)
	str := "abcdefghijklmnopqrstuvwxyz"
	for i := 1; i <= len(str); i++ {
		k := fmt.Sprintf("%d", i)
		m[k] = str[i-1 : i]
	}

	log.SetFlags(log.Lshortfile)
	// n := "1234"
	n := "111"

	f([]string{}, n, 0)

}
