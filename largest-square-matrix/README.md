Given a binary matrix, find out the maximum size square sub-matrix with all non-zeroes.

A variation of this problem is to calculate the square sub-matrix with all 1s or all Trues.

TAGS: DYNAMIC_PROGRAMMING
