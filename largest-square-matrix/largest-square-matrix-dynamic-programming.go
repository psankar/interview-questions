package main

import "log"

func min2(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func min(a, b, c int) int {
	x := min2(a, b)
	y := min2(x, c)

	return y
}

func main() {
	a := [4][5]int{
		[5]int{1, 2, 0, 1, 0},
		[5]int{0, 1, 2, 3, 0},
		[5]int{1, 4, 5, 6, 0},
		[5]int{0, 7, 8, 9, 1},
	}

	s := [4][5]int{}

	for i := 0; i < len(a[0]); i++ {
		if a[0][i] != 0 {
			s[0][i] = 1
		} else {
			s[0][i] = 0
		}
	}

	prevMax := -1

	for i := 1; i < len(a); i++ {
		for j := 1; j < len(a[0]); j++ {
			if a[i][j] != 0 {
				s[i][j] = min(s[i-1][j-1], s[i-1][j], s[i][j-1]) + 1
				if s[i][j] > prevMax {
					prevMax = s[i][j]
				}
			}
		}
	}

	log.Println(prevMax)
}
