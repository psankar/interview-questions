package main

import (
	"fmt"
	"log"
)

func findLongest(a [4][5]int, i, j int) int {
	log.Println(i, j)
	s := 0
outer:
	for {
		l := i + s
		m := j + s

		if l > rows || m > cols {
			break
		}

		for p := i; p < l; p++ {
			for q := i; q < m; q++ {
				t := a[p][q]
				fmt.Print(t, "\t")
				if t == 0 {
					fmt.Println()
					break outer
				}
			}
			fmt.Println()
		}

		s++
	}

	log.Println("\t\t", s-1, "\n\n")

	return s - 1
}

var rows, cols int

func main() {
	a := [4][5]int{
		[5]int{1, 2, 0, 1, 0},
		[5]int{0, 1, 2, 3, 0},
		[5]int{1, 4, 5, 6, 0},
		[5]int{0, 7, 8, 9, 1},
	}

	rows = len(a)
	cols = len(a[0])

	prevMax := -1

	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if a[i][j] != 0 {
				len := findLongest(a, i, j)

				if len > prevMax {
					prevMax = len
				}
			}
		}
	}

	log.Println("Maximum is: ", prevMax)
}
