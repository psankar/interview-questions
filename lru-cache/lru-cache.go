package main

import (
	"container/list"
	"sync"
)

type kv struct {
	key int
	val int
}

func (i *kv) Value() *kv {
	return i
}

type LRUCache struct {
	list     *list.List
	capacity int
	rwLock   sync.Mutex
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		list:     list.New(),
		capacity: capacity,
	}
}

func (this *LRUCache) Get(key int) int {
	this.rwLock.Lock()
	defer this.rwLock.Unlock()

	for e := this.list.Front(); e != nil; e = e.Next() {
		item := e.Value.(kv)
		if item.key == key {
			t := item.val
			this.list.MoveToFront(e)
			return t
		}
	}

	return -1
}

func (this *LRUCache) Put(key int, value int) {

	newItem := kv{key: key, val: value}

	this.rwLock.Lock()
	defer this.rwLock.Unlock()
	for e := this.list.Front(); e != nil; e = e.Next() {
		item := e.Value.(kv)
		if item.key == key {
			e.Value = newItem
			this.list.MoveToFront(e)
			return
		}
	}

	// Element is not found so far
	this.list.PushFront(newItem)

	if this.list.Len() > this.capacity {
		e := this.list.Back()
		this.list.Remove(e)
		return
	}

}

func main() {
	obj := Constructor(2)
	obj.Put(2, 1)
	obj.Put(2, 2)
	obj.Get(2)
	obj.Put(1, 1)
	obj.Put(4, 1)
	obj.Get(2)
}
