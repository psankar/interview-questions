// 8:30 PM

package main

import "log"

var x = [...]int{1, 2}
var n = 4

func findPaths(soFar []int) {
	for i := 0; i < len(x); i++ {
		sum := 0
		for _, j := range soFar {
			sum += j
		}

		sum += x[i]

		if sum == n {
			expand := append([]int{}, soFar...)
			expand = append(expand, x[i])
			log.Println(expand)
		} else if sum > n {
			// no-op
		} else {
			var newList = append(soFar, x[i])
			findPaths(newList)
		}
	}
}

func main() {
	findPaths([]int{})
}

// 8:45 PM
