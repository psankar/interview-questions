package main

import (
	"log"
)

func main() {
	a := []int{3, 4, -1, 1}
	// a := []int{-1, -2, -3, -5}

	seg := 0

	// Move all negative elements to the left
	for i := 0; i < len(a); i++ {
		if a[i] <= 0 {
			t := a[i]
			a[i] = a[seg]
			a[seg] = t
			seg++
		}
	}
	log.Println("After moving all -ve numbers to the left: ", a, seg)

	for i := seg; i < len(a); i++ {
		t := a[i]
		if t < 0 {
			t *= -1
		}
		// log.Println(i, t)
		if a[t-1] > 0 {
			a[t-1] = -a[t-1]
		}
	}

	log.Println("After changing signs: ", a, seg)

	for i := seg; i < len(a); i++ {
		if a[i] > 0 {
			log.Println("Missing element is: ", i+1)
			return
		}
	}

	log.Println("Missing element is: ", len(a)-seg+1)
}
