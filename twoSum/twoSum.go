package main

import "log"

func main() {
	a := []int{10, 15, 3, 7}
	k := 17

	m := make(map[int]struct{})
	var dummy struct{}
	for i := 0; i < len(a); i++ {
		l := k - a[i]
		if _, found := m[l]; found {
			log.Println("TRUE")
			return
		}
		m[a[i]] = dummy
	}

	log.Println("FALSE")
	return
}
