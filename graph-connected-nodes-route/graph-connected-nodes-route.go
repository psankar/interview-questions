package main

import (
	"fmt"
)

type node struct {
	value int
	to    []*node
}

type pair struct {
	parent *node
	child  *node
}

func shortestPath(from, to *node) bool {
	fmt.Println(from.value, to.value)
	fmt.Println("----------------")

	route := []int{}

	q := []*pair{}
	q = append(q, &pair{child: from, parent: nil})

	// Key: node, Value: parent of the node in key
	m := make(map[*node]*node)
	var tail *node

loop:
	for len(q) > 0 {
		i := q[0].child

		m[i] = q[0].parent

		for _, j := range i.to {
			if j == to {
				route = append(route, j.value)
				tail = i
				break loop
			}

			if _, visited := m[j]; !visited {
				q = append(q, &pair{child: j, parent: i})
			}
		}
		q = q[1:]
	}

	if tail != nil {
		for tail != nil {
			route = append(route, tail.value)
			tail = m[tail]
		}

		for i := len(route) - 1; i >= 0; i-- {
			fmt.Println(route[i])
		}

		return true
	}

	return false
}

func main() {
	one := &node{value: 1}
	two := &node{value: 2}
	three := &node{value: 3}
	four := &node{value: 4}
	five := &node{value: 5}

	one.to = []*node{two, three}
	two.to = []*node{five}
	three.to = []*node{}
	four.to = []*node{one, three}
	five.to = []*node{four}

	fmt.Println(shortestPath(three, one))
	fmt.Print("\n\n\n")
	fmt.Println(shortestPath(five, two))
	fmt.Print("\n\n\n")
	fmt.Println(shortestPath(two, three))
}
