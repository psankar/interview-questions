package main

import (
	"fmt"
)

func isPalindrome(s string) bool {

	// log.Println("Checking isPalindrome: ", s)

	if _, found := m[s]; found {
		return true
	}

	for i := 0; i < len(s)/2; i++ {
		if s[i:i+1] != s[len(s)-i-1:len(s)-i] {
			return false
		}
	}

	return true
}

var m map[string]int

func longestPalindrome(s string) string {

	if len(s) == 1 {
		return s
	}

	m = make(map[string]int)

	for i := 0; i < len(s); i++ {
		for j := i + 1; j <= len(s); j++ {
			str := s[i:j]
			if isPalindrome(str) {
				m[str] = len(str)
			}
		}
	}

	// log.Println(m)

	prevMaxVal := -1
	prevMaxKey := ""
	for k, v := range m {
		if v > prevMaxVal {
			prevMaxKey = k
			prevMaxVal = v
		}
	}

	return prevMaxKey
}

func main() {
	fmt.Println(longestPalindrome("cbbd"))
}
