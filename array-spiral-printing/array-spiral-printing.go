package main

import (
	"fmt"
	"log"
)

func main() {
	a := [][]int{
		[]int{10, 11, 12, 13},
		[]int{14, 15, 16, 17},
		[]int{18, 19, 20, 21},
		[]int{22, 23, 24, 25},
	}

	t := 0
	l := 0
	b := len(a) - 1
	r := len(a[0]) - 1

	log.Println(t, l, r, b)

	for b > t && r > l {
		for i := l; i <= r; i++ {
			fmt.Println(a[t][i])
		}
		t++

		for i := t; i <= b; i++ {
			fmt.Println(a[i][r])
		}
		r--

		for i := r; i >= l; i-- {
			fmt.Println(a[b][i])
		}
		b--

		for i := b; i >= t; i-- {
			fmt.Println(a[i][l])
		}
		l++
	}
}
