Given a 2d input array, print it spirally. For example,

```
10 11 12 13
14 15 16 17
18 19 20 21
22 23 24 25
```

should print `10 11 12 13 17 21 25 24 23 22 18 14 15 16 20 19`

```
1 2 3 4 5
6 7 8 9 1
2 3 4 5 6
```

should print `1 2 3 4 5 1 6 5 4 3 2 6 7 8 9`

TAGS: ARRAYS
