// Started at: 1:47 pm

package main

import (
	"fmt"
	"reflect"
)

type node struct {
	Value int
	Left  *node
	Right *node
}

type binaryTree struct {
	root *node
}

func walk(i *node) []int {

	empty := []int{-1}

	var out []int

	if i.Left == nil && i.Right == nil {
		// Leaf node
		out = append(out, i.Value)
		out = append(out, empty...)
		out = append(out, empty...)
		return out
	}

	out = append(out, i.Value)

	if i.Left == nil {
		out = append(out, empty...)
	} else {
		out = append(out, walk(i.Left)...)
	}

	if i.Right == nil {
		out = append(out, empty...)
	} else {
		out = append(out, walk(i.Right)...)
	}

	return out
}

func (b *binaryTree) serialize() []int {
	return walk(b.root)
}

func rebuild(s []int, pos *int, parent *node) {

	if s[*pos] == -1 {
		parent.Left = nil
		*pos++
	} else {
		node := &node{Value: s[*pos]}
		parent.Left = node
		*pos++
		rebuild(s, pos, node)
	}

	if s[*pos] == -1 {
		parent.Right = nil
		*pos++
	} else {
		node := &node{Value: s[*pos]}
		parent.Right = node
		*pos++
		rebuild(s, pos, node)
	}
}

func deserializeBinaryTree(s []int) *binaryTree {
	if len(s) == 0 {
		return &binaryTree{nil}
	}
	pos := 1

	root := &node{Value: s[0]}
	if len(s) > 1 {
		rebuild(s, &pos, root)
	}

	return &binaryTree{root}
}

func main() {

	root := &node{
		Value: 1,
		Left: &node{
			Value: 2,
			Left: &node{
				Value: 4,
				Left:  nil,
				Right: &node{
					Value: 9,
					Left:  nil,
					Right: nil,
				},
			},
			Right: &node{
				Value: 5,
				Left:  nil,
				Right: nil,
			},
		},
		Right: &node{
			Value: 3,
			Left:  nil,
			Right: &node{
				Value: 6,
				Left: &node{
					Value: 7,
					Left:  nil,
					Right: nil,
				},
				Right: &node{
					Value: 8,
					Left:  nil,
					Right: nil,
				},
			},
		},
	}

	b1 := &binaryTree{root}
	s1 := b1.serialize()

	fmt.Println("Serialized binary tree: \t\t", s1)

	b2 := deserializeBinaryTree(s1)
	s2 := b2.serialize()
	fmt.Println("Re-Serialized binary tree: \t\t", s2)

	if reflect.DeepEqual(s1, s2) {
		fmt.Println("Success")
	} else {
		fmt.Println("De/Serialization failed")
	}
}
